﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public class Enemy : Entity,
                         IDamagable
    {
        [SerializeField]
        private int   currentHealth;

        [SerializeField]
        private float attack;

        [SerializeField]
        private bool  isAlive;

        public int  CurrentHealth
        {
            get { return currentHealth;  }
            set { currentHealth = value; }
        }

        public  float Attack
        {
            get { return attack;  }
            set { attack = value; }
        }

        public  bool IsAlive
        {
            get { return isAlive;  }
            set { isAlive = value; }
        }

        public void Damage(int damageAmount)
        {
            if (currentHealth > 0)
            {
                currentHealth -= damageAmount;
            }
            else if (currentHealth <= 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Log("Name: " + EntityName + " LP: " + CurrentHealth);
                }

            if (currentHealth <= 0)
            {
                 currentHealth = 0;
                 isAlive = false;
                 Log("Name: " + EntityName + " LP: " + CurrentHealth);
            }

            Log(EntityName + " was damaged and is now at " + CurrentHealth + " LP.");
        }
    }
}
