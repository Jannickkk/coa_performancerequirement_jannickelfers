﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public class EntityManager : MonoBehaviour,
                                 ILog
    {
        public void Log(object message)
        {
            Debug.Log(message);
        }

        [SerializeField]
        private Entity[] entities;

        public Entity[] Entities
        {
            get { return entities;  }
            set { entities = value; }
        }

        private void Start()
        {
            Log("Registrating names of the entities.");
            for (int i = 0; i < Entities.Length; i++)
            {
                Log("Loop is currently at: " + i + ". Currently registrating Entity: " + Entities[i].EntityName);
            }
        }
    }
}
