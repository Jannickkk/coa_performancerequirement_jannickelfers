﻿namespace PRONE
{
    public interface IAttack
    {
        void AttackEnemy(Enemy enemyToAttack, int attackDamage);
    }
    
}