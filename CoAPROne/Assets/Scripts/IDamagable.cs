﻿namespace PRONE
{
    public interface IDamagable
    {
        void Damage(int damageAmount);
    }
}