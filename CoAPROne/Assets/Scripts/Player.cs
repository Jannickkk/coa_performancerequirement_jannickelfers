﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PRONE
{
    public class Player : Entity,
                          IAttack
    {
        [SerializeField]
        private int   currentHealth;

        [SerializeField]
        private int attack;

        [SerializeField]
        private bool  isAlive;

        [SerializeField]
        private Enemy enemyEntity;

        public int CurrentHealth
        {
            get { return currentHealth;  }
            set { currentHealth = value; }
        }

        public int Attack
        {
            get { return attack;  }
            set { attack = value; }
        }

        public bool IsAlive
        {
            get { return isAlive;  }
            set { isAlive = value; }
        }

        public Enemy EnemyEntity
        {
            get { return enemyEntity;  }
            set { enemyEntity = value; }
        }

        public void AttackEnemy(Enemy enemyToAttack, int attackDamage)
        {
            if (isAlive == true)
            {
                enemyToAttack.Damage(attackDamage);
            }
        }

        private void Start()
        {
            Log("Name of Player: " + EntityName);
            Log("Lifepoints: "     + CurrentHealth);
            Log("Attack: "         + Attack);
            AttackEnemy(enemyEntity, attack);
        }
    }
}