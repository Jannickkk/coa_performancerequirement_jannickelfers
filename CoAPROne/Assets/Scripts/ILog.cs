﻿namespace PRONE
{
    public interface ILog
    {
        void Log(object message);
    }
}
